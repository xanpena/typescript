console.log('Hello, TypeScript');

// ***************************
// TIPOS BÁSICOS 
// ***************************

// Boolean
let muted: boolean = true;
muted = false;
//muted = "forzando conversión a string"; // Error

// Números
let numerador: number = 42;
let denominador: number = 6;
let resultado = numerador / denominador;

// String
let nombre: string = 'Xan';
let saludo = `Me llamo ${nombre}`;

// Arrays
let people: string[] = [];
people = ["Isabel", "Raúl", "Lola"];
// people.push(9000); // Error

let peopleAndNumbers : Array< string | number > = [];
peopleAndNumbers = ["Isabel", "Raúl", "Lola"];
peopleAndNumbers.push(9000);

// Enum
enum Color {
    Rojo = "Rojo",
    Verde = "Verde",
    Azul = "Azul"
}

let colorFavorito: Color = Color.Verde;
console.log(`Mi color favorito es ${colorFavorito}`); // 1 // Verde

// Any
let comodin: any = "joker";
comodin = { type: "Wilcard" }

// Object
let someObject: object = { type: "Wilcard" }

// ***************************
// FUNCIONES
// ***************************

function add(a: number, b: number): number{
    return a + b;
}

const sum = add(2, 3);

function createAdder (a: number): (number) => number{
    return function (b: number){
        return b + a;
    }
}

const addFour = createAdder(4)
const fourPlus6 = addFour(6)

function fullName(firstName: string, lastName?: string): string { 
    return `${firstName} ${lastName}`;
}

const xan = fullName("Xan");

// ***************************
// INTERFACES
// ***************************

interface Rectangulo {
    ancho: number
    alto: number
}

let rect : Rectangulo = {
    ancho: 4,
    alto: 6
}

function area(r: Rectangulo): number {
    return r.alto * r.ancho;
}

const areaRect = area(rect);
console.log(areaRect);